Hello, thanks for checking out the source for my new tab Chrome extension for Lichess!  You can find this extension on the [Chrome Web Store](https://chrome.google.com/webstore/detail/lichess-new-tab/dboknomlhdmhmfoonfoibiakaeclfjna).

Be sure to visit and support Lichess at [lichess.org](https://lichess.org).  It's an amazing site and community; anything you'd have given me as thanks for this extension please give to them instead.

I know a lot of this code isn't really optimized, but it works, and it was more an experiment to see if I could actually build this.  If you have ideas for improvements just let me know or submit a merge request. 

Icons made by Eucalyp from flaticon.com licensed by CC 3.0 BY
