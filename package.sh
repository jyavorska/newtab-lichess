#!/usr/bin/env bash

rm newtab-lichess.zip
zip newtab-lichess embed.js icon16.png icon32.png icon64.png icon128.png manifest.json lichess-newtab.html
